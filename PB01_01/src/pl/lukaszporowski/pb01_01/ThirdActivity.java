package pl.lukaszporowski.pb01_01;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebView;
import android.widget.TextView;

public class ThirdActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_third);
		Intent newintent = getIntent();
		String adresUrl = newintent.getExtras().getString("AdresURL");
		TextView textview2 = (TextView)findViewById(R.id.textView2);
		textview2.setText(adresUrl);
		WebView webview = (WebView)findViewById(R.id.webView1);
		webview.loadUrl(adresUrl);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.third, menu);
		return true;
	}
}

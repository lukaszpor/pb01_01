package pl.lukaszporowski.pb01_01;

import android.app.Activity;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends Activity {
	
	public final static String EXTRA_MESSAGE = "pl.lukaszporowski.pb01_01.MESSAGE";
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		setupActionBar();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	public void startExternal(View view)
	{
		EditText editText = (EditText)findViewById(R.id.adres);
		String adresUrl = editText.getText().toString();
		if (adresUrl.equals(""))
			adresUrl = editText.getHint().toString();
		Uri webpage = Uri.parse(adresUrl);
		Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
		startActivity(webIntent);
		return;
	}
	
	public void startMyBrowser(View view)
	{
		EditText editText = (EditText)findViewById(R.id.adres);
		String adresUrl = editText.getText().toString();
		if (adresUrl.equals(""))
			adresUrl = editText.getHint().toString();
		Intent newintent = new Intent(this, ThirdActivity.class);
		newintent.putExtra("AdresURL", adresUrl);
		startActivity(newintent);
		return;
	}
}
